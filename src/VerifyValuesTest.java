import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class VerifyValuesTest {

    @Test
    public void verifyStringValue() {
        Demo d = new Demo();
        String actualValue = d.returnStringValue();
        String expectedValue = "Hello World";
        assertEquals (actualValue, expectedValue);
    }
}